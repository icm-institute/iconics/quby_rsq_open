# MA plot description

The MA-plot provides a global view of the differential genes, with the log2 fold change on the y-axis over the mean of normalized counts on the x-axis. On the left are low expressed genes, and high expressed genes on the right.

Points colored in orange are up-regulated in group2 vs group1 ; points colored in blue are down-regulated in group2 vs group1.


